FactoryGirl.define do
  factory :child do
    name { Faker::StarWars.character }
    grade { Faker::Number.between(0, 3) }
    bday { Faker::Time.between(4.years.ago, 15.years.ago)  }
  end
end