FactoryGirl.define do
  factory :device do
  	title { Faker::Name.name }
    token { Faker::Crypto.sha256 }
  end
end
