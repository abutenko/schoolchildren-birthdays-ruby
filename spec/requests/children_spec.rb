# spec/requests/children_spec.rb
require 'rails_helper'

RSpec.describe 'Children API', type: :request do
  # initialize test data 
  let!(:children) { create_list(:child, 10) }

  # Test suite for GET /children
  describe 'GET /children' do
    # make HTTP get request before each example
    before { get '/children' }

    it 'returns children' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /recent_birthdays
  describe 'GET /recent_birthdays' do
    # make HTTP get request before each example
    before { get '/recent_birthdays' }
    
    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end

RSpec.describe 'Children API with excel data', type: :request do
  # don't initialize test data, let pull data from excel file

  # Test suite for GET /children
  describe 'GET /children' do
    # make HTTP get request before each example
    before { get '/children' }

    it 'returns children' do
      expect(json).not_to be_empty
      expect(json.size).to eq(50)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end