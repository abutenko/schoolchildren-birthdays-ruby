class CreateChildren < ActiveRecord::Migration[5.1]
  def change
    create_table :children do |t|
      t.string :name
      t.string :grade
      t.date :bday

      t.timestamps
    end
  end
end
