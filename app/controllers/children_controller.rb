class ChildrenController < ApplicationController
  def initialize
	if Child.count == 0
		get_children_from_excel
	end  	
  end

  def get_children_from_excel
	# Load FI data e.g. Apple credentials from .xlsx file to retrieve fiserv apple id and iTC/Dev portal team id
	file_path = "#{Rails.root}/config/Schoolchildren.xlsx"

	if !File.exist?(file_path)
		ap "The excel spreadsheet supplied #{file_path} either doesn't exist or is not a .xlsx file", :color => {:string => :red}
		exit 1
	end
	excel = Roo::Excelx.new(file_path) # https://github.com/roo-rb/roo
	if excel.first_row.nil?
		ap "The excel spreadsheet supplied #{file_path} is empty.", :color => {:string => :red}
		exit 1
	end
	expected_row_headers = ["Grade", "Name", "Phone", "DOB", "email", "Baptized"]
	if excel.row(excel.first_row) != expected_row_headers
		ap "The excel spreadsheet supplied does not have the correct column headers. Please ensure the headers exactly match these values:", :color => {:string => :red}
		ap "#{expected_row_headers}", :color => {:string => :red}
		exit 1
	end

	result = excel.parse(grade: 'Grade', name: 'Name', bday: 'DOB') 
	Child.create(result) 
  end

  # GET /children
  def index
    @children = Child.all
    json_response(@children)
  end

  # GET /recent_birthdays
  def recent_birthdays
  	children_with_bdays = Child.children_with_recent_birthday
    json_response(children_with_bdays)
  end
end
