class DevicesController < ApplicationController
  # POST /devices
  def create
    @device = Device.create!(params.permit(:title, :token))
    json_response(@device, :created)
  end
end
