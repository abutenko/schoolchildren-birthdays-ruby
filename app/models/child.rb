class Child < ApplicationRecord

validates_presence_of :name

def self.children_with_recent_birthday
	return Child.where("cast(bday + ((extract(year from age(bday))) * interval '1' year) as date) >= current_date - 6")
end

end
