require 'fcm'

desc "This task is called by the Heroku scheduler add-on"
task :check_birthdays => :environment do
  children_with_bdays = Child.children_with_recent_birthday
  message = "#{children_with_bdays.count} person had their birthdays recently"
  ap message, :color => {:string => :green}

  #TODO: send notification if there were some birthdays
  server_key = Rails.application.secrets.fcm_server_key
  unless server_key.nil?
  	ap "server key: " + server_key, :color => {:string => :green}
  	fcm = FCM.new(server_key)

    tokens = Device.pluck("token")
    ap "tokens: " + tokens.join(','), :color => {:string => :green}
  	options = {data: {message: message}, priority: 'high'}
  	response = fcm.send(tokens, options)
  	ap "response: " + response.inspect
  else 
	ap "server key not found", :color => {:string => :red}
  end
end

