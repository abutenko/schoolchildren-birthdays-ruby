Rails.application.routes.draw do
	root to: 'children#recent_birthdays' 
	get 'children', to: 'children#index'
	get 'recent_birthdays', to: 'children#recent_birthdays'
	post 'devices', to: 'devices#create'
end